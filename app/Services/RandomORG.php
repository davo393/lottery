<?php
namespace App\Services;

use Graze\GuzzleHttp\JsonRpc\Client;

class RandomORG
{
    public function generateIntegers()
    {
        $client = Client::factory('https://api.random.org/json-rpc/1/invoke');
        $integers = $client->send($client->request(123, 'generateSignedIntegers', ['apiKey'=>env('RANDOM_ORG_API_KEY'), 'n'=>9, 'min'=>1, 'max'=>9, 'replacement'=>false]));
        return $integers->getRpcResult();
    }
}