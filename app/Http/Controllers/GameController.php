<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RandomORG;
use App\Game;

class GameController extends Controller
{

    private $prizeArray = [1,5,10,20,30,50,100,200,250];

    public function index(Request $request){

        $request->validate([
            'cell_id' => 'required|integer|in:1,2,3,4,5,6,7,8,9',
        ]);

        $randomClient = new RandomORG();
        $randomResponse = $randomClient->generateIntegers();
        $randomIntegers = $randomResponse['random']['data'];
        $signature =  $randomResponse['signature'];
        $prize = $this->prizeArray[$randomIntegers[$request->cell_id-1]-1];

        $dataForDb = [
            'generated_numbers' => json_encode($randomIntegers),
            'chosen_number' => $request->cell_id,
            'prize' =>  $prize,
            'json_response' => json_encode($randomResponse),
            'signature' => $signature
        ];

        if(Game::create($dataForDb)){

            $data = [
                "cell1" => $this->prizeArray[$randomIntegers[0]-1],
                "cell2" => $this->prizeArray[$randomIntegers[1]-1],
                "cell3" => $this->prizeArray[$randomIntegers[2]-1],
                "cell4" => $this->prizeArray[$randomIntegers[3]-1],
                "cell5" => $this->prizeArray[$randomIntegers[4]-1],
                "cell6" => $this->prizeArray[$randomIntegers[5]-1],
                "cell7" => $this->prizeArray[$randomIntegers[6]-1],
                "cell8" => $this->prizeArray[$randomIntegers[7]-1],
                "cell9" => $this->prizeArray[$randomIntegers[8]-1],
            ];

            return response()->json(['status' => 200, 'data' => $data]);
        }else{
            return response()->json(['status' => 500, 'error' => ['message' => 'Непредвиденная ошибка на сервере! Попробуйте позже.']], 500);
        }
    }

    public function listGames(){
        $games = Game::all();
        return response()->json(['status' => 200, 'data' => $games]);
    }
}
