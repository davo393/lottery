- composer install
- Copy .env.example to .env file
- change db credentials and add RANDOM_ORG_API_KEY in .env file
- php artisan migrate

##Routes
#Auth
- Sign up - POST - /api/auth/signup
- Login - POST - /api/auth/login
- Logout - GET - /api/auth/logout (*must be authenticated)

#Game

- Play game - POST - /api/game (*must be authenticated)
- List games - GET - /api/games/list (*must be authenticated)
